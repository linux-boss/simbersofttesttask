package simbersoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;

import static org.junit.jupiter.api.Assertions.*;

class URLConnectionReaderTest {

    @Test
    void readStringFromURL() throws Exception {
        Assertions.assertEquals("Test string"
                , URLConnectionReader.readStringFromURL("file://"
                            + System.getProperty("user.dir")
                            + "/data.tst"));
    }

    @Test
    void fakeURL() {
        Throwable thrown = assertThrows(MalformedURLException.class
                , () -> URLConnectionReader.readStringFromURL("fakeURL"));
        assertNotNull(thrown.getMessage());
    }
}