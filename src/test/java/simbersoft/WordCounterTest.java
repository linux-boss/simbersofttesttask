package simbersoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

class WordCounterTest {

    @Test
    void count() {
        Assertions.assertEquals(new HashMap<String, Integer>() {
            {
                put("One", 1);
                put("Two", 2);
                put("Three", 3);
            }
        }, WordCounter.count(":\"One:Two:Two?Three\tThree\nThree\r"));
    }

    @Test
    void emptyData() {
        Assertions.assertEquals(new HashMap<String, Integer>(), WordCounter.count(""));
    }
}