package simbersoft;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionReader {
    /**
     * Из заданого URL-а загружает данные
     * @param url заданный ресурс
     * @return Скаченные данные в виде строки
     * @throws Exception в результате ошибок при работе с ресурсом URL
     */
    public static String readStringFromURL(String url) throws Exception {
        URL urlData = new URL(url);
        URLConnection yc = urlData.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()));
        String inputLine;
        StringBuilder outputLine = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            outputLine.append(inputLine);
        in.close();
        return outputLine.toString();
    }
}
