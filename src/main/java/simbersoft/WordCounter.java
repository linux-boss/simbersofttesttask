package simbersoft;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class WordCounter {
    /**
     * Подсчёт слов в строке по списку разделителей
     * @param inString входная строка
     * @return Мапа в которой key - слово во входной строке, value - число вхождений слова в строку, пустая мапа,
     * если входных данных нет
     */
    public static Map<String, Integer> count(String inString) {
        if (inString.isEmpty()) return new HashMap<>();
        String[] temp = inString.split(" +|,+|\\.+|!+|\\?+|\"+|;+|:+|\\[+|]+|\\(+|\\)+|\\n+|\\r+|\\t+");
        Map<String, Integer> result = new HashMap<>();
        for (String str : temp) {
            // Пропускаем пустые строки. Возможно, надо доработать Regex
            if (str.isEmpty()) continue;
            if (result.computeIfPresent(str, (key, value) -> value + 1) == null)
                result.put(str, 1);
        }
        // Для сортировки по убыванию указать -entry.getValue()
        return result.entrySet()
                .stream()
                .sorted(Comparator.comparingInt(entry -> entry.getValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (k, v) -> { throw new AssertionError();},
                        LinkedHashMap::new
                ));
    }
}
