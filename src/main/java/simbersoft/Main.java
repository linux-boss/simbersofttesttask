package simbersoft;

public class Main {
    public static void main(String[] args) {
        if (args.length == 1) {
            System.out.println(args[0]);
            try {
                WordCounter.count(URLConnectionReader.readStringFromURL(args[0]))
                        .forEach((key, value) ->
                        System.out.println(key + " - " + value));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        else {
            System.err.println("Need an url like this https://www.simbirsoft.com/");
            System.exit(1);
        }
    }
}
